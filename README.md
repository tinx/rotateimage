# RotateImage

This is a simple Java app / library that can be utilized to automatically 
adjust the orientation of an image according to it's embedded EXIF metadata.

Please note that the image is re-encoded and thus the process is not lossless.

## Compile

Execute:

```
mvn package
```

This will drop the executable JAR and it's dependencies into the `target` 
directory.
You will need `rotate-image-SOME_VERSION.jar` and the `lib` directory.


## Usage

If you are using the command line, just execute it like:

```
java -jar target/rotate-image-SOME_VERSION.jar /path/to/image.jpg [...]
```
