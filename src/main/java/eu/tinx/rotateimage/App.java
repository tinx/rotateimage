package eu.tinx.rotateimage;

import java.io.File;

/**
 * This is a simple command line interface for {@code RotateImage}.
 * </br></br>
 * {@code RotateImage} rotates and flips/mirrors images according to their EXIF metadata.
 * </br></br>
 * For example images see:
 *   {@code https://github.com/recurser/exif-orientation-examples}
 */
public class App 
{
    public static void main( String[] args )
    {
        if (args.length < 1) {
            System.err.println("Pass at least one image path as commandline argument!");
            return;
        }

        for (String path : args) {
            if (path.isEmpty()) continue;

            File f = new File(path);
            System.out.print( "Processing file '" + f.getPath() + "'... ");
            if (!f.canRead()) {
                System.err.println("\nError: Cannot read from file '" + f.getPath() + "'");
                return;
            }
            try {
                RotationService.process(f, new File(f.getPath() + "_rotated.jpg"));
            } catch (Exception e) {
                System.err.println("\nError: I/O error while processing file '" + f.getPath() + "'");
//                e.printStackTrace();
            }
            System.out.println("Done!");
        }
    }
}
