package eu.tinx.rotateimage;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;

import javax.imageio.ImageIO;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * This is the rotation service for {@code RotateImage}. This is where the magic happens!
 * </br></br>
 * {@code RotateImage} rotates and flips/mirrors images according to their EXIF metadata.
 * </br></br>
 * For example images see:
 *   {@code https://github.com/recurser/exif-orientation-examples}
 */
public class RotationService {
    final static String OUTPUT_FORMAT = "jpg";

    /**
     * Automatically adjusts (rotates, flips / mirrors) the image according to their EXIF orientation data.
     *
     * @param fIn Input {@code File}, containing the image to adjust.
     * @param fOut Output {@code File}, where the adjusted image is written.
     * @throws IOException if there is a problemreading / writing given {@code File}s.
     * @throws ImageProcessingException if there is a problem reading EXIF metadata.
     */
    public static void process (File fIn, File fOut) throws IOException, ImageProcessingException {
        final BufferedImage img = ImageIO.read(fIn);
        final int imgWidth = img.getWidth();
        final int imgHeight = img.getHeight();
        Orientation orientation = getOrientation(fIn);
        AffineTransform transformation = null;
        if (orientation.deg != 0) {
            transformation = getRotationTransform(orientation, imgWidth, imgHeight);
        }
        if (orientation.flip_h || orientation.flip_v) {
            if (transformation == null)
                transformation = getFlipTransform(orientation, imgWidth, imgHeight);
            else
                transformation.concatenate(getFlipTransform(orientation, imgWidth, imgHeight));
        }

        BufferedImage targetBuffer;
        if (transformation != null) {
            targetBuffer = getRotationBuffer(orientation, imgWidth, imgHeight, img.getType());

            AffineTransformOp transOp = new AffineTransformOp(transformation, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
            transOp.filter(img, targetBuffer);
        } else {
            targetBuffer = img;
        }

        ImageIO.write(targetBuffer, OUTPUT_FORMAT, fOut);
    }

    /**
     * Prepares the flip / mirror transformation according to given orientation information.
     *
     * @param o The {@code Orientation} read from EXIF image metadata.
     * @param width The original image width.
     * @param height The original image height.
     * @return The transformation that can be used to flip / mirror the image.
     */
    private static AffineTransform getFlipTransform(final Orientation o, final int width, final int height) {
        AffineTransform trans = null;
        if (o.flip_h)  {
            trans = AffineTransform.getScaleInstance(-1, 1);
            trans.concatenate(AffineTransform.getTranslateInstance(-width, 0));
        }
        if (o.flip_v) {
            if (trans == null) {
                trans = AffineTransform.getScaleInstance(1, -1);
            } else {
                trans.concatenate(AffineTransform.getScaleInstance(1, -1));
            }
            trans.concatenate(AffineTransform.getTranslateInstance(0, -height));
        }
        return trans;
    }

    /**
     * Prepares the rotation transformation according to given orientation information.
     *
     * @param o The {@code Orientation} read from EXIF image metadata.
     * @param width The original image width.
     * @param height The original image height.
     * @return The transformation that can be used to rotate the image.
     */
    private static AffineTransform getRotationTransform(final Orientation o, final int width, final int height) {
        if (o.deg == 0) {
            return null;
        }

        double rotationCenterX;
        double rotationCenterY;

        switch (o.deg) {
            case 90:
                rotationCenterX = height / 2;
                rotationCenterY = height / 2;
                break;
            case 270:
                rotationCenterX = width / 2;
                rotationCenterY = width / 2;
                break;
            default:
                rotationCenterX = width / 2;
                rotationCenterY = height / 2;
        }

        return AffineTransform.getRotateInstance(
                o.rad,
                rotationCenterX,
                rotationCenterY
        );
    }

    /**
     * Prepares the target image buffer for the rotation transformation according to given orientation information.
     *
     * @param o The {@code Orientation} read from EXIF image metadata.
     * @param width The original image width.
     * @param height The original image height.
     * @param type The buffer type of the original image.
     * @return The buffer which can be used to write the rotated image into.
     */
    private static BufferedImage getRotationBuffer(final Orientation o, final int width, final int height, final int type) {
        int newWidth, newHeight;

        switch (o.deg) {
            case 90:
                newWidth = height;
                newHeight = width;
                break;
            case 270:
                newWidth = height;
                newHeight = width;
                break;
            default:
                newWidth = width;
                newHeight = height;
        }

        return new BufferedImage(newWidth, newHeight, type);
    }

    /**
     * Reads the EXIF metadata {@code Orientation} field from the given file.
     *
     * @param imgF The image file containing EXIF metadata.
     * @return The {@code Oorientation} of the image according to EXIF metadata.
     * @throws IOException if there is a problem accessing the image file.
     * @throws ImageProcessingException if there is a problem processing EXIF metadata,
     *                                  e.g. if there is no {@code Orientation} field there.
     */
    public static Orientation getOrientation (File imgF) throws IOException, ImageProcessingException {
        Metadata meta = ImageMetadataReader.readMetadata(imgF);
        String rawOrientation = null;
        for (Directory dir : meta.getDirectories()) {
            rawOrientation = dir.getString(ExifSubIFDDirectory.TAG_ORIENTATION);
            if (rawOrientation != null && !rawOrientation.isEmpty()) {
                break;
            }
        }

        Orientation o = Orientation.valueOf(Integer.parseInt(rawOrientation));
        if (o != Orientation.NOT_SPECIFIED) {
            return o;
        }

        throw new ImageProcessingException("EXIF Orientation not specified.");
    }
}
