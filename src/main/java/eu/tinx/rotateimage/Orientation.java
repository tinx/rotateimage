package eu.tinx.rotateimage;

/**
 * Orientation according to EXIF standard.
 */
public enum Orientation {
    NOT_SPECIFIED (0),
    KEEP (1),
    FLIP_HORIZONTALLY (2),
    ROTATE_180 (3),
    FLIP_VERTICALLY (4),
    ROTATE_270_FLIP_HORIZONTALLY (5),
    ROTATE_90 (6),
    ROTATE_90_FLIP_HORIZONTALLY (7),
    ROTATE_270 (8);


    /**
     * The rotation degree for the image transformation.
     * Correct values are always positive.
     */
    public final int deg;

    /**
     * Determines if the image should be flipped horizontally.
     */
    public final boolean flip_h;

    /**
     * Determines if the image should be flipped vertically.
     */
    public final boolean flip_v;

    /**
     * The deg radians for the image transformation.
     */
    public final double rad;

    /**
     * The {@code Orientation} in EXIF integer representation.
     */
    public final int value;

    Orientation (int i) {
        value = i;

        if (value == 2 || value == 5 || value == 7) {
            flip_h = true;
            flip_v = false;
        } else if (value == 4) {
            flip_h = false;
            flip_v = true;
        } else {
            flip_h = false;
            flip_v = false;
        }

        switch (value) {
            case 1: deg = 0; break;
            case 2: deg = 0; break;
            case 3: deg = 180; break;
            case 4: deg = 0; break;
            case 5: deg = 270; break;
            case 6: deg = 90; break;
            case 7: deg = 90; break;
            case 8: deg = 270; break;
            default: deg = -1;
        }

        rad = Math.toRadians(deg);
    }

    /**
     * Get the {@code Orientation} matching the EXIF integer representation.
     *
     * @param exifOrientation EXIF compliant integer value of the Orientation.
     * @return Matching {@code Orientation}.
     */
    public static Orientation valueOf(int exifOrientation) {
        switch (exifOrientation) {
            case 1: return KEEP;
            case 2: return FLIP_HORIZONTALLY;
            case 3: return ROTATE_180;
            case 4: return FLIP_VERTICALLY;
            case 5: return ROTATE_270_FLIP_HORIZONTALLY;
            case 6: return ROTATE_90;
            case 7: return ROTATE_90_FLIP_HORIZONTALLY;
            case 8: return ROTATE_270;
        }
        return NOT_SPECIFIED;
    }
}
